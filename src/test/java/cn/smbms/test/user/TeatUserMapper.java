package cn.smbms.test.user;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.Test;

import cn.smbms.dao.user.UserMapper;
import cn.smbms.pojo.Address;
import cn.smbms.pojo.User;
import cn.smbms.tools.MyBatisUtil;

public class TeatUserMapper {
	private Logger  logger =Logger.getLogger(TeatUserMapper.class);
	
	@Test
    public void testGetCount(){
    	int count = 0;
    	SqlSession sqlSession = null;
    	try {
			//1 创建sqlSession
			sqlSession = MyBatisUtil.creataSqlSession();
			//2 使用getMapper方式,需要先创建interface方法,且interface方法名和mapper.xml中id保持一致
			count = sqlSession.getMapper(UserMapper.class).getCount();
			logger.debug("COUNT等于:---->"+count);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			//3 关闭sqlSession对象
			MyBatisUtil.closeSqlSession(sqlSession);
		}
    }
	
	//单参数查询
	@Test
    public void testGetUserListByUserName(){
		List<User> userlist= null;
    	SqlSession sqlSession = null;
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			userlist = sqlSession.getMapper(UserMapper.class).getUserListByUserName("明");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
    	for(User user:userlist){
			logger.debug(user.getAddress()+"\t"+user.getUserName());
    	}
    }
	
	//多参数查询(封装成实体)
	@Test
    public void testGetUserListByUser(){
		User user = new User();
		user.setUserName("明");
		user.setUserRole(2);
		List<User> userlist= null;
    	SqlSession sqlSession = null;
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			userlist = sqlSession.getMapper(UserMapper.class).getUserListByUser(user);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
    	for(User _user:userlist){
			logger.debug(_user.getAddress()+"\t"+_user.getUserName());
    	}
    }
	
	//多参数查询(使用Map)
	@Test
    public void testGetUserListByMap(){
		Map<String,String> userMap=new HashMap<String, String>();
		userMap.put("userName", "明");
		userMap.put("userRole", "2");
		List<User> userlist= null;
    	SqlSession sqlSession = null;
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			userlist = sqlSession.getMapper(UserMapper.class).getUserListByMap(userMap);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
    	for(User _user:userlist){
			logger.debug(_user.getAddress()+"\t"+_user.getUserName());
    	}
    }
	
//	@Test
//	public void testGetUserList(){
//		User user = new User();
//		user.setUserName("明");
//		user.setUserRole(2);
//		List<User> userlist= null;
//    	SqlSession sqlSession = null;
//    	try {
//			sqlSession = MyBatisUtil.creataSqlSession();
//			//userlist = sqlSession.getMapper(UserMapper.class).getUserList(user);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally{
//			MyBatisUtil.closeSqlSession(sqlSession);
//		}
//    	for(User _user:userlist){
//			logger.debug(_user.getAddress()+"\t"+_user.getUserName()+"\t"+_user.getUserRoleName()+"\t"+_user.getAge());
//    	}
//    }
	
	@Test
	public void testAdd(){
    	SqlSession sqlSession = null;
    	int count =0;
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			User user = new User();
			user.setUserCode("teaper");
			user.setUserName("茶巴");
			user.setUserPassword("1234567");
			Date birthday=new SimpleDateFormat("yyyy-MM-dd").parse("2017-5-6");
			user.setBirthday(birthday);
			user.setAddress("测试新增用户");
			user.setGender(1);
			user.setPhone("14588345567");
			user.setUserRole(1);
			user.setCreatedBy(1);
			user.setCreationDate(new Date());
			count = sqlSession.getMapper(UserMapper.class).add(user);
			//模拟异常,进行回滚
			sqlSession.commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			sqlSession.rollback();
			count=0;
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
		logger.debug("是否添加成功------>"+count);
    }
	
	@Test
	public void testModify(){
    	SqlSession sqlSession = null;
    	int count =0;
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			User user = new User();
			user.setId(16);
			user.setUserCode("teaper");
			user.setUserName("湘桥月");
			user.setUserPassword("123456");
			Date birthday=new SimpleDateFormat("yyyy-MM-dd").parse("2017-5-6");
			user.setBirthday(birthday);
			user.setAddress(null);
			user.setGender(1);
			user.setPhone("1458ee45567");
			user.setUserRole(1);
			user.setModifyBy(1);
			user.setModifyDate(new Date());
			count = sqlSession.getMapper(UserMapper.class).modify(user);
			//模拟异常,进行回滚
			sqlSession.commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			sqlSession.rollback();
			count=0;
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
		logger.debug("是否修改成功------>"+count);
    }
	
	@Test
	public void testDeleteUserById(){
    	SqlSession sqlSession = null;
    	int count =0;
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			count = sqlSession.getMapper(UserMapper.class).deleteUserById(16);
			//模拟异常,进行回滚
			sqlSession.commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			sqlSession.rollback();
			count=0;
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
		logger.debug("是否删除成功------>"+count);
    }
	
//	@Test
//	public void testGetUserListByRoleId(){
//		List<User> userlist= null;
//    	SqlSession sqlSession = null;
//    	Integer roleId=2;
//    	try {
//			sqlSession = MyBatisUtil.creataSqlSession();
//			userlist = sqlSession.getMapper(UserMapper.class).getUserListByRoleId(roleId);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally{
//			MyBatisUtil.closeSqlSession(sqlSession);
//		}
//    	for(User _user:userlist){
//			logger.debug(_user.getUserName()+"\t"+_user.getRole().getId()+"\t"
//    	+_user.getRole().getRoleCode()+"\t"+_user.getRole().getRoleName());
//    	}
//    }
	
	@Test
	public void testGetAddressListByUserId(){
		List<User> userlist= null;
    	SqlSession sqlSession = null;
    	Integer userId=1;
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			userlist = sqlSession.getMapper(UserMapper.class).getAddressListByUserId(userId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
    	for(User _user:userlist){
			logger.debug(_user.getUserName()+"\t"+_user.getUserCode()+"\t"+_user.getUserPassword());
			for(Address address:_user.getAddressList()){
				logger.debug(address.getId()+"\t"+address.getContact()+"\t"+address.getAddressDesc()
				+"\t"+address.getTel()+"\t"+address.getPostCode());
			}
    	}
    }
	
	//if+where多条件查询
//	@Test
//	public void testGetUserList(){
//		List<User> userlist= null;
//    	SqlSession sqlSession = null;
//    	String userName="明";
//    	Integer RoleId=null;
//    	try {
//			sqlSession = MyBatisUtil.creataSqlSession();
//			userlist = sqlSession.getMapper(UserMapper.class).getUserList(userName,RoleId);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally{
//			MyBatisUtil.closeSqlSession(sqlSession);
//		}
//    	for(User _user:userlist){
//    		logger.debug(_user.getAddress()+"\t"+_user.getUserName()+"\t"+_user.getUserRoleName()+"\t"+_user.getAge());
//    	}
//    }
	
	@Test
	public void testGetUserList_choose(){
		List<User> userlist= null;
    	SqlSession sqlSession = null;
    	String userName="明";
    	Integer roleId=null;
    	String userCode="";
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			Date creationDate =new SimpleDateFormat("yyyy-MM-dd").parse("2014-12-31");
			userlist = sqlSession.getMapper(UserMapper.class).getUserList_choose(userName, roleId, userCode, creationDate);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
    	for(User _user:userlist){
    		logger.debug(_user.getUserName()+"\t"+_user.getUserRole()+"\t"+_user.getCreationDate());
    	}
    }
	
	//入参为数组类型的foreach迭代
	@Test
	public void testGetUserListByRoleId_foreach_array(){
		List<User> userlist= null;
    	SqlSession sqlSession = null;
    	Integer[] roleIds={2,3};
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			userlist = sqlSession.getMapper(UserMapper.class).getUserListByRoleId_foreach_array(roleIds);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
    	for(User _user:userlist){
    		logger.debug(_user.getId()+"\t"+_user.getUserCode()+"\t"
    				+_user.getUserName()+"\t"+_user.getUserRole());
    	}
    }
	@Test
	public void testGetUserListByRoleId_foreach_list(){
		List<User> userlist= null;
    	SqlSession sqlSession = null;
    	List<Integer> roleList=new ArrayList<Integer>();
    	roleList.add(2);
    	roleList.add(3);
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			userlist = sqlSession.getMapper(UserMapper.class).getUserListByRoleId_foreach_list(roleList);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
    	for(User _user:userlist){
    		logger.debug(_user.getId()+"\t"+_user.getUserCode()+"\t"
    				+_user.getUserName()+"\t"+_user.getUserRole());
    	}
    }
	
	@Test
	public void testGetUserListByRoleId_foreach_map(){
		List<User> userlist= null;
    	SqlSession sqlSession = null;
    	Map<String,Object> conditionMap=new HashMap<String, Object>();
    	List<Integer> roleList=new ArrayList<Integer>();
    	roleList.add(2);
    	roleList.add(3);
    	conditionMap.put("gender", 1);
    	conditionMap.put("roleIds", roleList);
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			userlist = sqlSession.getMapper(UserMapper.class).getUserListByRoleId_foreach_map(conditionMap);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
    	for(User _user:userlist){
    		logger.debug(_user.getId()+"\t"+_user.getUserCode()+"\t"
    				+_user.getUserName()+"\t"+_user.getUserRole());
    	}
    }
	
	//分页
	@Test
	public void testGetUserList(){
		List<User> userlist= null;
    	SqlSession sqlSession = null;
    	String userName=null;
    	Integer roleId=null;
    	Integer pageSize=5;
    	Integer currentPageNo=0;
    	try {
			sqlSession = MyBatisUtil.creataSqlSession();
			userlist = sqlSession.getMapper(UserMapper.class).getUserList(userName, roleId, currentPageNo, pageSize);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			MyBatisUtil.closeSqlSession(sqlSession);
		}
    	for(User _user:userlist){
    		logger.debug(_user.getId()+"\t"+_user.getUserCode()+"\t"+_user.getUserName()+"\t"+_user.getUserRoleName()
    		+"\t"+_user.getAge());
    	}
    }
}
