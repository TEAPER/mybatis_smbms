package cn.smbms.dao.user;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;

import cn.smbms.pojo.User;
@Resource
public interface UserMapper {
	//登录
	public User getLoginUser(@Param("userCode")String userCode) throws Exception;
	
	public List<User> getUserListByUserName(@Param("userName") String userName) throws Exception;
	
	public List<User> getUserListByUser(User user) throws Exception;
	
	//public List<User> getUserList(User user) throws Exception;
	
	//if+where多条件查询
	//public List<User> getUserList(@Param("userName") String userName,@Param("userRole") Integer RoleId) throws Exception;
	
	public List<User> getUserListByMap(Map<String,String> userMap) throws Exception;
	
	//查询所有用户信息
//	public List<User> getUserList(@Param("userName") String userName,
//	@Param("userRole") int userRole,
//	@Param("currentPageNo") int currentPageNo, 
//	@Param("pageSize") int pageSize)throws Exception;
	
	
	//查询记录数
	public int getUserCount(@Param("userName") String userName,@Param("userRole") int userRole) throws Exception;
	
	//添加(insert)
	public int add(User user) throws Exception;
	
	public User getUserById(@Param("id")int id) throws Exception; 
	
	//修改用户信息
	public int modify(User user) throws Exception;
	
	//删除用户
	public int deleteUserById(@Param("id")int id)throws Exception;
	
	//根据用户ID查询用户信息(association)
	public List<User> getUserListByRoleId(@Param("userRole")Integer roleId) throws Exception;
	
	//获取指定用户的相关信息和地址列表(collection)
	public List<User> getAddressListByUserId(@Param("id")Integer userId);
	
	//入参为数组类型的foreach迭代
	public List<User> getUserListByRoleId_foreach_array(Integer[] roleIds)throws Exception;
	public List<User> getUserListByRoleId_foreach_list(List<Integer> roleList)throws Exception;
	public List<User> getUserListByRoleId_foreach_map(Map<String,Object> conditionMap)throws Exception;
	
	//choose(where,otherwise)
	public List<User> getUserList_choose(@Param("userName")String userName,
			@Param("userRole")Integer roleId,
			@Param("userCode")String userCode,
			@Param("creationDate")Date creationDate)throws Exception;
	
	//无条件查询记录数
	public int getCount() throws Exception;
	/**
	 * 查询用户列表(分页)
	 * getCount() 之前已写过,获取总记录数
	 * @param userName 用户名
	 * @param roleId 角色编号
	 * @param currentPageNo  起始位置
	 * @param pageSize 页面大小
	 */
	public List<User> getUserList(@Param("userName") String userName,
			@Param("userRole") Integer roleId,
			@Param("from") Integer currentPageNo,
			@Param("pageSize") Integer pageSize) throws Exception;
	
	
}
