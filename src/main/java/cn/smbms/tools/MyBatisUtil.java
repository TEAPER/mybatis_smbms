package cn.smbms.tools;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MyBatisUtil {
	private static SqlSessionFactory factory;
	static{//静态代码块,factory只创建一次
		try {
			InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
			factory = new SqlSessionFactoryBuilder().build(is);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static SqlSession creataSqlSession(){
		return factory.openSession(false);//true为自动提交事务
	}
	public static void closeSqlSession(SqlSession sqlSession){
		if(null != sqlSession)
			sqlSession.close();
	}
	

}
